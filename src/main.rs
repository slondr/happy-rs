// Compute the perfect digital invariant
fn pdi(mut number: i64) -> i64 {
    let mut total = 0;
    while number > 0 {
        total += (number % 10).pow(2);
        number /= 10;
    }
    total
}

fn is_happy(number: i64) -> bool {
    let mut slow = number;
    let mut fast = number;

    loop {
        slow = pdi(slow);
        fast = pdi(pdi(fast));
        if slow == fast {
            break;
        }
    }
    slow == 1
}

fn main() {
    // test known happy numbers
    let known_happy_numbers = [
        1, 7, 13, 19, 23, 31, 79, 97, 103, 193, 239, 409, 487, 563, 617, 653, 673, 683, 709, 739,
        761, 863, 881, 907, 937, 1009, 1033, 1039, 1093, 1151, 1277, 1303, 1373, 1427, 1447, 1481,
        1487, 1511, 1607, 1663,
    ];
    for happy_number in known_happy_numbers.iter() {
        assert!(is_happy(*happy_number), "{} should be happy", happy_number);
    }
    // test known unhappy numbers
    let known_unhappy_numbers = [8, 14, 20, 410, 411, 412, 486];
    for unhappy_number in known_unhappy_numbers.iter() {
        assert_eq!(
            is_happy(*unhappy_number),
            false,
            "{} should not be happy",
            unhappy_number
        );
    }

    // command line args
    let command_line_args = std::env::args().collect::<Vec<String>>();
    let args = command_line_args.iter().skip(1);
    for arg in args {
        let number: i64 = arg
            .parse()
            .expect(format!("Unable to parse {} as float", arg).as_str());
        println!("{}", if is_happy(number) { "happy" } else { "sad" });
    }
}
