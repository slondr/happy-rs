happy-rs is a simple Rust program to compute if a given number is happy or sad.

Give it a number as a command line arg, and it will tell you.
